package dataGeneration;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import nearestNeigh.Category;
import nearestNeigh.Point;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class DataGeneration {
	private double minLat = -35;
	private double maxLat = -39;
	private double minLong = 139;
	private double maxLong = 149;
	List<Point> pointsList;

	public DataGeneration(int size) {
		pointsList = new ArrayList<Point>();
		
		int datasetSize = size;
		
		for (int i=0; i < datasetSize; i++){
			int rand = randBetween(0,2);
			Category cat = Category.values()[rand];
			String id = "id" + i;
			Random random = new Random();
			double lat = minLat + (random.nextDouble() * (maxLat - minLat + 1));
			double lon = minLong + (random.nextDouble() * (maxLong - minLong + 1));
			Point p = new Point(id, cat, lat, lon);
			pointsList.add(p);
		}
	}
	
	public List<Point> getPointsList(){
		return this.pointsList;
	}
	
	
	public int randBetween(int min, int max) {
		Random r = new Random();
		int randNum = r.nextInt((max - min) + 1 ) + min;
		return randNum;
	}
	
	public void createFile(String file)
            throws IOException {
            	FileWriter writer = new FileWriter(file + ".txt");
                for (int i = 0; i < this.pointsList.size(); i++ ) {
                	writer.write(this.pointsList.get(i).id + " ");
                	writer.write(this.pointsList.get(i).cat + " ");
                	writer.write(String.valueOf(this.pointsList.get(i).lat) + " ");
                	writer.write(String.valueOf(this.pointsList.get(i).lon));
                    if(i < this.pointsList.size()-1)
                        writer.write("\n");
                }
                writer.close();
            }
}
