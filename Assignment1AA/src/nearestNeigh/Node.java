package nearestNeigh;

public class Node {

	private Point data;
	private Node left;
	private Node right;
	private char splittingAxis;
	
	public Node(Point data) {
		this.data = data;
		left = null;
		right = null;
	}
	
	public void setData(Point data) {
		this.data = data;
	}
	
	public Point getData() {
		return this.data;
	}
	
	public void setLeftNode(Node left) {
		this.left = left;
	}
	
	public void setRightNode(Node right) {
		this.right = right;
	}
	
	public Node getLeftNode() {
		return this.left;
	}
	
	public Node getRightNode() {
		return this.right;
	}
	
	public char getSplittingAxis() {
		return this.splittingAxis;
	}
	
	public void setSplittingAxis(char axis) {
		this.splittingAxis = axis;
	}
}
