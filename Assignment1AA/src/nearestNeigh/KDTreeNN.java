package nearestNeigh;

import java.util.ArrayList;
import java.util.List;


/**
 * This class is required to be implemented.  Kd-tree implementation.
 *
 * @author Jeffrey, Youhan
 */
public class KDTreeNN implements NearestNeigh {
	
	private Node root;
	private static final String LEFT_NODE = "left";
	private static final String RIGHT_NODE = "right";
	
	public KDTreeNN () {
		root = null;
	}

    @Override
    public void buildIndex(List<Point> points) {

    	//Sorts all points passed in, based on axis using a quicksort algorithm.
    	List<Point> sortedPoints = new ArrayList<Point>();
    	sortedPoints = quickSort(0, points.size() - 1, 'x', points);

    	//Subsets of values for use in determining left and right child nodes
    	List<Point> lowerValues = new ArrayList<Point>();
    	List<Point> higherValues = new ArrayList<Point>();

    	//Finds initial median point of sorted points to set as root node.
    	Point medianPoint;
    	if (sortedPoints.size() % 2 == 0) {
    		
    		int medianValue = sortedPoints.size()/2 - 1;
    		
    		for(int i = 0; i < medianValue; i++) {
    			lowerValues.add(sortedPoints.get(i));
    		}
    		
    		for (int j = medianValue + 1; j < sortedPoints.size() ;j++) {
    			higherValues.add(sortedPoints.get(j));
    		}
    		
    		medianPoint = sortedPoints.remove(medianValue);
    	} else {
    		int medianValue = (int)Math.floor(sortedPoints.size()/2 - 1);
    		
    		
    		for(int i = 0; i < medianValue; i++) {
    			lowerValues.add(sortedPoints.get(i));
    		}
    		
    		for (int j = medianValue + 1; j < sortedPoints.size() ;j++) {
    			higherValues.add(sortedPoints.get(j));
    		}
    		
    		medianPoint = sortedPoints.remove(medianValue);
    	}
    	Node rootNode = new Node(medianPoint);
    	setRootNode(rootNode);
    	
    	/*
    	 * Calls the generateTree method on both subsets of lower and higher values to find
    	 * median point for child node of root.
    	*/
    	Node leftChild = generateTree(lowerValues, 'x');
    	Node rightChild = generateTree(higherValues, 'x');
    	root.setLeftNode(leftChild);
    	root.setRightNode(rightChild);
    	root.setSplittingAxis('x');
    }

    @Override
    public List<Point> search(Point searchTerm, int k) {

    	List<Point> nearbyPoints = new ArrayList<Point>();
        Node currentNode = root;
    	char splittingAxis = '?';
    	nearbyPoints = performNNSearch(splittingAxis, currentNode, searchTerm, k, nearbyPoints);
        return nearbyPoints;
    }

    @Override
    public boolean addPoint(Point point) {

    	boolean addSuccess = false;
    	
    	if(isPointIn(point)) {
    		return addSuccess;
    	}
    	
    	Node currentNode = root;
        char splittingAxis = '?';
        Node parentNode = currentNode;
        String childNodeId= null;
        
        do {
        	if (splittingAxis == 'y') {
        		splittingAxis = 'x';
        		if (point.lat < currentNode.getData().lat) {
        			parentNode = currentNode;
        			currentNode = currentNode.getLeftNode();
        			childNodeId = LEFT_NODE;
        		} else {
        			parentNode = currentNode;
        			currentNode = currentNode.getRightNode();
        			childNodeId = RIGHT_NODE;
        		}
        	} else if (splittingAxis == 'x') {
        		splittingAxis = 'y';
        		if (point.lon < currentNode.getData().lon) {
        			parentNode = currentNode;
        			currentNode = currentNode.getLeftNode();
        			childNodeId = LEFT_NODE;
        		} else {
        			parentNode = currentNode;
        			currentNode = currentNode.getRightNode();
        			childNodeId = RIGHT_NODE;
        		}
        	} else {
        		splittingAxis = 'x';
        		if (point.lat < currentNode.getData().lat) {
        			parentNode = currentNode;
        			currentNode = currentNode.getLeftNode();
        			childNodeId = LEFT_NODE;
        		} else {
        			parentNode = currentNode;
        			currentNode = currentNode.getRightNode();
        			childNodeId = RIGHT_NODE;
        		}
        	}

        } while (currentNode != null);
        
        if(currentNode == null) {
        	Node newNode = new Node(point);
        	if (childNodeId == RIGHT_NODE) {
        		parentNode.setRightNode(newNode);
        	} else {
        		parentNode.setLeftNode(newNode);
        	}
        	addSuccess = true;
        }
        return addSuccess;
    }

    @Override
    public boolean deletePoint(Point point) {

    	boolean deleteSuccess = false;
    	List<Point> pointsForTreeRebuild = new ArrayList<Point>();

    	//Performs tree rebuild is root is to be deleted.
    	if(point.equals(root.getData())) {
    		pointsForTreeRebuild = getChildNodesForRebuild(root, pointsForTreeRebuild);
    		buildIndex(pointsForTreeRebuild);
    		
    	//Performs delete and rebuild if point is contained in tree and below root node.
    	} else {
        	Node returnedParentNode = getParentOfNodeToDelete('?', root, point);
        	if (returnedParentNode != null) {
        		if(returnedParentNode.getLeftNode() != null) {
        			if(returnedParentNode.getLeftNode().getData().equals(point)) {
        				pointsForTreeRebuild = getChildNodesForRebuild(returnedParentNode.getLeftNode(), pointsForTreeRebuild);
            			returnedParentNode.setLeftNode(null);
            			Node newLeftNode = rebuildAfterDeletion(pointsForTreeRebuild, returnedParentNode);
            			returnedParentNode.setLeftNode(newLeftNode);
            			deleteSuccess = true;
            		} else if (returnedParentNode.getRightNode() != null) {
            			if(returnedParentNode.getRightNode().getData().equals(point)) {
            				pointsForTreeRebuild = getChildNodesForRebuild(returnedParentNode.getRightNode(), pointsForTreeRebuild);
            				returnedParentNode.setRightNode(null);
            				Node newRightNode = rebuildAfterDeletion(pointsForTreeRebuild, returnedParentNode);
        					returnedParentNode.setRightNode(newRightNode);
            				deleteSuccess = true;
            			}
            		}
        		} else {
        			if (returnedParentNode.getRightNode() != null) {
        				if(returnedParentNode.getRightNode().getData().equals(point)) {
        					pointsForTreeRebuild = getChildNodesForRebuild(returnedParentNode.getRightNode(), pointsForTreeRebuild);
        					returnedParentNode.setRightNode(null);
        					Node newRightNode = rebuildAfterDeletion(pointsForTreeRebuild, returnedParentNode);
        					returnedParentNode.setRightNode(newRightNode);
        					deleteSuccess = true;
        				}
        			}
        		}
        	} 	
    	}
    	return deleteSuccess;
    }

    @Override
    public boolean isPointIn(Point point) {

    	List<Point> nearbyPoints = new ArrayList<Point>();
    	performNNSearch('?', root, point, 1, nearbyPoints);
    	for(Point pointInList : nearbyPoints ) {
    		if (point.equals(pointInList) ){
        		return true;
        	}
    	}
        return false;
    }
    
    private void setRootNode(Node root) {
    	this.root = root;
    }
    
    //Quicksort algorithm for sorting based on axis. 
    private List<Point> quickSort(int low, int high, char axis, List<Point> unsortedPoints) {
    	
    	 
    	Point[] pointsArray = new Point[unsortedPoints.size()];
    	for(int i = 0; i < pointsArray.length; i++) {
    		pointsArray[i] = unsortedPoints.get(i);
    	}
    	
    	
    	int i = low;
    	int j = high;
    	Point pivotPoint = pointsArray[(int)Math.floor(low + (high - low)/2)];
    	
    	double pivot;
    	if(axis == 'x') {
    		pivot = pivotPoint.lat;
    		
    		while(i <= j) {
        		while(pointsArray[i].lat < pivot) {
        			i++;
        		}
        		
        		while(pointsArray[j].lat > pivot) {
        			j--;
        		}
        		
        		if(i <= j) {
        			Point temp = pointsArray[i];
        			pointsArray[i] = pointsArray[j];
        			pointsArray[j] = temp;
        			i++;
        			j--;
        		}		
        	} 
    	} else {
    		pivot = pivotPoint.lon;
    		
    		while(i <= j) {
        		while(pointsArray[i].lon < pivot) {
        			i++;
        		}
        		
        		while(pointsArray[j].lon > pivot) {
        			j--;
        		}
        		
        		if(i <= j) {
        			Point temp = pointsArray[i];
        			pointsArray[i] = pointsArray[j];
        			pointsArray[j] = temp;
        			i++;
        			j--;
        		}		
        	} 
    	}
    	
    	unsortedPoints.clear();
    	for(int n = 0; n < pointsArray.length; n++) {
    		unsortedPoints.add(pointsArray[n]);
    	}
    	
    	if(low < j) {
    		quickSort(low, j, axis, unsortedPoints);
    	}
    	
    	if(i < high) {
    		quickSort(i, high, axis, unsortedPoints);
    	}
    	
    	return unsortedPoints;
    }
 
    // Method for creating the tree based on a list of points supplied to it.
    private Node generateTree(List<Point> points, char prevSplittingAxis) {
    	
    	//Alternates the splitting axis
    	char thisSplittingAxis;
    	if (prevSplittingAxis == 'x') {
    		thisSplittingAxis = 'y';
    	} else {
    		thisSplittingAxis = 'x';
    	}

    	//Sorts passed in points based on the alternating axis.
    	List<Point> sortedPoints = quickSort(0, points.size() - 1, thisSplittingAxis, points);
    	List<Point> lowerValues = new ArrayList<Point>();
    	List<Point> higherValues = new ArrayList<Point>();
    	
    	Point medianPoint;
    	if (sortedPoints.size() % 2 == 0) {
    		
    		int medianValue = sortedPoints.size()/2 - 1;
    		
    		for(int i = 0; i < medianValue; i++) {
    			lowerValues.add(sortedPoints.get(i));
    		}
    		
    		for (int j = medianValue + 1; j < sortedPoints.size() ;j++) {
    			higherValues.add(sortedPoints.get(j));
    		}
    		
    		medianPoint = sortedPoints.remove(medianValue);
    	} else {
    		int medianValue = (int)Math.floor(sortedPoints.size()/2 - 1);
    		
    		if(medianValue < 0) {
    			medianValue = 0;
    		}
    		
    		for(int i = 0; i < medianValue; i++) {
    			lowerValues.add(sortedPoints.get(i));
    		}
    		
    		for (int j = medianValue + 1; j < sortedPoints.size() ;j++) {
    			higherValues.add(sortedPoints.get(j));
    		}
    		medianPoint = sortedPoints.remove(medianValue);
    	}
    	
    	Node node = new Node(medianPoint);
    	Node leftChild;
    	Node rightChild;
    	
    	//Decides how child nodes should be set based on remaining values in array subsets.
    	if(lowerValues.size() < 1) {
    		leftChild = null;
    	} else if (lowerValues.size() == 1) {
    		leftChild = new Node(lowerValues.get(0));
    	} else {
    		//Calls recursively if multiple values remaining.
    		leftChild = generateTree(lowerValues, thisSplittingAxis); 
    	}
    	
    	if (higherValues.size() < 1) {
    		rightChild = null;
    	} else if (higherValues.size() == 1) {
    		rightChild = new Node (higherValues.get(0));
    	} else {
    		rightChild = generateTree(higherValues, thisSplittingAxis);
    	}
    	
    	node.setLeftNode(leftChild);
    	node.setRightNode(rightChild);
    	node.setSplittingAxis(thisSplittingAxis);
    	
    	//Node returned to the previous recursive call to set its children appropriately.
    	return node;
    }
    
    /*
     * Searching Algorithm for finding and returning k number of nearest neighbors
     * to search term passed in.
     */
    private List<Point> performNNSearch(char splittingAxis, Node currentNode, 
    		Point searchTerm, int k, List<Point> nearbyPoints) {
    	
    	Node nextNode;
    	String childNodeId;
    	
    	//For working way down KD-Tree.
    	if (splittingAxis == 'y') {
    		splittingAxis = 'x';
    		if (searchTerm.lat < currentNode.getData().lat) {
    			nextNode = currentNode.getLeftNode();
    			childNodeId = LEFT_NODE;
    		} else {
    			nextNode = currentNode.getRightNode();
    			childNodeId = RIGHT_NODE;
    		}
    	} else if (splittingAxis == 'x') {
    		splittingAxis = 'y';
    		if (searchTerm.lon < currentNode.getData().lon) {
    			nextNode = currentNode.getLeftNode();
    			childNodeId = LEFT_NODE;
    		} else {
    			nextNode = currentNode.getRightNode();
    			childNodeId = RIGHT_NODE;
    		}
    	} else {
    		splittingAxis = 'x';
    		if (searchTerm.lat < currentNode.getData().lat) {
    			nextNode = currentNode.getLeftNode();
    			childNodeId = LEFT_NODE;
    		} else {
    			nextNode = currentNode.getRightNode();
    			childNodeId = RIGHT_NODE;
    		}
    	}

    	//Once last node is hit, will stop recursively calling and move on, and add node to list if appropriate.
    	if (nextNode != null) {
    		performNNSearch(splittingAxis, nextNode, 
    				searchTerm, k, nearbyPoints);
    	}
    	
    	checkToAddCurrentNodeToList(k, searchTerm, currentNode, nearbyPoints);
    	
    	//Check the current node to see if other subtree could hold closer value.
    	//Create a temporary point that holds values to test against.
		Point altPoint;
		
    	if (splittingAxis == 'x') {
    		altPoint = new Point();
    		altPoint.lat = currentNode.getData().lat;
    		altPoint.lon = searchTerm.lon;
    	} else {
    		altPoint = new Point();
    		altPoint.lat = searchTerm.lat;
    		altPoint.lon = currentNode.getData().lon;
    	}
    	
    	if(nearbyPoints.size() > 0 && nearbyPoints.size() == k) {
    		Point currentFurthest = nearbyPoints.get(0);
        	for (Point point: nearbyPoints) {
        		if(point.distTo(searchTerm) > currentFurthest.distTo(searchTerm)) {
    				 currentFurthest = point;
        		}
        	}
        	if(altPoint.distTo(searchTerm) < currentFurthest.distTo(searchTerm)) {
        		if(childNodeId.equals(LEFT_NODE)) {
            		nextNode = currentNode.getRightNode();
            	} else {
            		nextNode = currentNode.getLeftNode();
            	}
        		if(nextNode != null) {
        			performNNSearch(splittingAxis, nextNode, 
            				searchTerm, k, nearbyPoints);
        		}
        		checkToAddCurrentNodeToList(k, searchTerm, currentNode, nearbyPoints);
        	}
    	} else {
        	if(childNodeId.equals(LEFT_NODE)) {
           		nextNode = currentNode.getRightNode();
           	} else {
           		nextNode = currentNode.getLeftNode();
           	}
       		if(nextNode != null) {
       			performNNSearch(splittingAxis, nextNode, 
           				searchTerm, k, nearbyPoints);
       		}
       		checkToAddCurrentNodeToList(k, searchTerm, currentNode, nearbyPoints);
        	
    	}
    	return nearbyPoints;
    }
    
    /*
     * Searches through tree to locate node to be deleted and returns that nodes parent 
     * if node to be deleted is contained within tree. Allowing tree to be rebuilt from
     * the parent node down.
     */
    private Node getParentOfNodeToDelete(char splittingAxis, Node currentNode, 
    		Point searchTerm){
    	
    	Node parentNode = null;
    	Node nextNode;

    	if (splittingAxis == 'y') {
    		splittingAxis = 'x';
    		if (searchTerm.lat < currentNode.getData().lat) {
    			nextNode = currentNode.getLeftNode();
    		} else {
    			nextNode = currentNode.getRightNode();
    		}
    	} else if (splittingAxis == 'x') {
    		splittingAxis = 'y';
    		if (searchTerm.lon < currentNode.getData().lon) {
    			nextNode = currentNode.getLeftNode();
    		} else {
    			nextNode = currentNode.getRightNode();
    		}
    	} else {
    		splittingAxis = 'x';
    		if (searchTerm.lat < currentNode.getData().lat) {
    			nextNode = currentNode.getLeftNode();
    		} else {
    			nextNode = currentNode.getRightNode();
    		}
    	}
    	
    	if(nextNode != null) {
    		if(nextNode.getData().equals(searchTerm)) {
    			parentNode = currentNode;
    		} else {
    			parentNode = getParentOfNodeToDelete(splittingAxis, nextNode, 
        				searchTerm);
        	}
    	} 

    	return parentNode;
    }
    
    //Checks if point should be added to List, returns list with any appropriate updates
    private List<Point> checkToAddCurrentNodeToList(int numberOfPointsToFind, Point searchTerm, Node currentNode,
    		List<Point> points) {
    	
    	if(currentNode.getData().cat.equals(searchTerm.cat)) {
    		if(points.size() > 0) {
    			for (Point point : points) {
       			 if (currentNode.getData().equals(point)) {
       				 return points;
       			 }
       		 }
    		}
    		if (points.size() == numberOfPointsToFind) {
        		Point currentFurthest = points.get(0);
        		 for (Point point : points) {
        			 if (point.distTo(searchTerm) > currentFurthest.distTo(searchTerm)) {
        				 currentFurthest = point;
        			 }
        		 }
        		 if(currentFurthest.distTo(searchTerm) > currentNode.getData().distTo(searchTerm)) {
        			 points.remove(currentFurthest);
        			 points.add(currentNode.getData());
        			 return points;
        		 }
        		 
        	} else {
        		points.add(currentNode.getData());
        		return points;
        	}
    	}
    	return points;
    }
    
    //Returns a list of points for rebuilding tree after node deletion.
    private List<Point> getChildNodesForRebuild(Node currentNode, List<Point> points) {
    	
    	Node nextNode;
    	nextNode = currentNode.getLeftNode();

    	/*
    	 * Keeps moving down left side of node being deleted filling array with all points until
    	 * it reaches a null value, then checks right node at bottom and moves down its left side until
    	 * that reaches a null value, recursively moving back up the tree and repeating till the start 
    	 * is reached and returns the now filled array.
    	 */
    	if(nextNode != null) {
    		points.add(nextNode.getData());
    		getChildNodesForRebuild(nextNode, points);
    	} 
    	
    	nextNode = currentNode.getRightNode();
    	
    	if(nextNode != null) {
    		points.add(nextNode.getData());
    		getChildNodesForRebuild(nextNode, points);
    	}
    	return points;
    }
    
    /*
     * Rebuilds tree below deletion and returns the top node to be placed as a child of the 
     * parent of the deleted node.
    */
    private Node rebuildAfterDeletion(List<Point> arrayOfPointsToRebuild, Node newParentNode ) {
    	
    	//To check if anything needs to be rebuilt, eg. wont execute if deleted node was leaf.
    	if(arrayOfPointsToRebuild.size() > 0) {
    		
    		char thisSplittingAxis;
        	if (newParentNode.getSplittingAxis() == 'x') {
        		thisSplittingAxis = 'y';
        	} else {
        		thisSplittingAxis = 'x';
        	}
    		
    		List<Point> sortedPoints = quickSort(0, arrayOfPointsToRebuild.size() - 1, thisSplittingAxis, arrayOfPointsToRebuild);
        	List<Point> lowerValues = new ArrayList<Point>();
        	List<Point> higherValues = new ArrayList<Point>();

        	Point medianPoint;
        	if (sortedPoints.size() % 2 == 0) {
        		
        		int medianValue = sortedPoints.size()/2 - 1;
        		
        		for(int i = 0; i < medianValue; i++) {
        			lowerValues.add(sortedPoints.get(i));
        		}
        		
        		for (int j = medianValue + 1; j < sortedPoints.size() ;j++) {
        			higherValues.add(sortedPoints.get(j));
        		}
        		medianPoint = sortedPoints.remove(medianValue);
        	} else {
        		int medianValue = (int)Math.floor(sortedPoints.size()/2 - 1);
        		
        		if(medianValue < 0) {
        			medianValue = 0;
        		}
        		
        		for(int i = 0; i < medianValue; i++) {
        			lowerValues.add(sortedPoints.get(i));
        		}
        		
        		for (int j = medianValue + 1; j < sortedPoints.size() ;j++) {
        			higherValues.add(sortedPoints.get(j));
        		}
        		medianPoint = sortedPoints.remove(medianValue);
        	}
        	
        	Node node = new Node(medianPoint);
        	Node leftChild;
        	Node rightChild;
        	
        	if(lowerValues.size() < 1) {
        		leftChild = null;
        	} else if (lowerValues.size() == 1) {
        		leftChild = new Node(lowerValues.get(0));
        	} else {
        		leftChild = generateTree(lowerValues, thisSplittingAxis); 
        	}
        	
        	if (higherValues.size() < 1) {
        		rightChild = null;
        	} else if (higherValues.size() == 1) {
        		rightChild = new Node (higherValues.get(0));
        	} else {
        		rightChild = generateTree(higherValues, thisSplittingAxis);
        	}
        	
        	node.setLeftNode(leftChild);
        	node.setRightNode(rightChild);
        	node.setSplittingAxis(thisSplittingAxis);
        	
        	return node;
    	} else {
    		return null;
    	}

    }
}

