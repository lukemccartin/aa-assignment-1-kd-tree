package nearestNeigh;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is required to be implemented.  Naive approach implementation.
 *
 * @author Jeffrey, Youhan
 */
public class NaiveNN implements NearestNeigh {
	
	List<Point> naivePoints;

	public NaiveNN() {
		naivePoints = new ArrayList<Point>();
	}
	
    @Override
    public void buildIndex(List<Point> points) {
        for (Point point : points) {
        	naivePoints.add(point);
        }
    }

    @Override
    public List<Point> search(Point searchTerm, int k) {
    	
        List<Point> tempArray = new ArrayList<Point>(naivePoints);
        List<Point> nearestPoints = new ArrayList<Point>();
        Point currentSmallest = null;
        
        /* Remove the searchTerm from the array if present 
         * so as not to include it in nearest points list.
         */
        for (Point tempPoint : tempArray) {
        	if (tempPoint.id == searchTerm.id) {
        		tempArray.remove(tempPoint);
        	}
        }
        
        
        while (nearestPoints.size() < k) {
        	currentSmallest = tempArray.get(0);
        	for (Point tempPoint : tempArray) {
        		if(tempPoint.cat == searchTerm.cat) {
        			if (tempPoint.distTo(searchTerm) < currentSmallest.distTo(searchTerm)) {
                		currentSmallest = tempPoint;
                	}
        		}
            	
            }
        	tempArray.remove(currentSmallest);
            nearestPoints.add(currentSmallest);
        }
        return nearestPoints;
    }

    @Override
    public boolean addPoint(Point point) {
    	
    	for (Point existingPoint : naivePoints) {
    		if (existingPoint.id.equals(point.id)) {
    			return false;
    		}
    	}
    	naivePoints.add(point);
        return true;
    }

    @Override
    public boolean deletePoint(Point point) {

        for (Point existingPoint : naivePoints) {
        	if (existingPoint.id.equals(point.id)) {
        		naivePoints.remove(existingPoint);
        		return true;
        	}
        }
        return false;
    }

    @Override
    public boolean isPointIn(Point point) {
        for (Point existingPoint : naivePoints) {
        	if (existingPoint.id.equals(point.id)) {
        		return true;
        	}
        }
        return false;
    }

}
