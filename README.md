# README #

Subject: Algorithms and Analysis
Project: Construct both a brute-force and Kd-Tree approach to handling 'n' number of nearest neighbour requests in relation to latitude and longitude based data.
	Test efficiency of both approaches and compile a report with findings.
Team Structure: Pair based. Luke McCartin and Melissa Jones.

Code breakdown:

src/nearestNeighFileBased.java  - All code within this class was pre-written and provided to students by teaching staff for use with testing and evaluation. No further implementation was required of students and thus no code contained within was the work of any student member of this team.
src/sampleData.txt - Sample data provided by teaching staff for use with testing.

src/dataGeneration/DataGeneration.java - All work attributed to Melissa Jones.

src/nearestNeigh/Category.java - Provided code, no student implementation required.
src/nearestNeigh/Point.java - Provided code, no student implementation required.
src/nearestNeigh/NearestNeigh.java - Provided code, no student implementation required.
src/nearestNeigh/Node.java - Class created by and all work attributed to Luke McCartin.
src/nearestNeigh/NaiveNN.java - Skeleton class provided by teaching staff, all class/method implementation and work attributed to Luke McCartin.
src/nearestNeigh/KDTreeNN.java - Skeleton class provided by teaching staff, all class/method implementation and work attributed to Luke McCartin.

testing/ - all files excluding Scenario1.in, Scenario2.in, testData.10000.txt, testData.100000.txt and testData.50000.txt were provided by teaching staff for testing purposes. The aforementioned files were created using dataGeneration.java.

Final report not provided here.

After note: 

Majority of work was required within the KDTreeNN.java file, this class handled the creation of a KDTree using a Quicksort algorithm created by Luke McCartin, all searching, addition and deletion operations etc. For more information, please feel free to contact myself (Luke McCartin) at ldmccartin@gmail.com